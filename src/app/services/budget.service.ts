import {HttpClient} from '../app.interceptor';
import {Injectable} from '@angular/core';

@Injectable()
export class BudgetService {

  constructor(private _http: HttpClient) {
  }

  getAll(companyId, answered = false, isOpen = true) {
    return this._http.get('companies/' + companyId + `/budgets?answered=${answered}&isOpen=${isOpen}`);
  }

  getById(companyId, budgetId) {
    return this._http.get('companies/' + companyId + '/budgets/' + budgetId);
  }

  createBudgetResponse(data) {
    return this._http.post('budgetResponses', JSON.stringify(data));
  }
}
