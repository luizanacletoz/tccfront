import {HttpClient} from '../app.interceptor';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {

  constructor(private _http: HttpClient) {
  }

  info() {
    return this._http.get('accounts/info');
  }

  auth(data) {
    return this._http.post('accounts/auth', JSON.stringify(data));
  }

  changePassword(data) {
    return this._http.put('accounts/password', JSON.stringify(data));
  }
}
