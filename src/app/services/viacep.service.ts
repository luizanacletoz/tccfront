import {HttpClient} from '../app.interceptor';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class ViacepService {

  constructor(private _http: Http) {
  }

  get(data) {
    return this._http.get(`https://viacep.com.br/ws/${data}/json/`);
  }
}
