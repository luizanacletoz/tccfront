import {HttpClient} from '../app.interceptor';
import {Injectable} from '@angular/core';

@Injectable()
export class CreditService {

  constructor(private _http: HttpClient) {
  }

  sendData(data) {
    return this._http.post('credits', JSON.stringify(data));
  }
}
