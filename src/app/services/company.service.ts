import {HttpClient} from '../app.interceptor';
import {Injectable} from '@angular/core';

@Injectable()
export class CompanyService {

  constructor(private _http: HttpClient) {
  }

  register(data) {
    return this._http.post('accounts/companies', JSON.stringify(data));
  }

  put(data) {
    return this._http.put('accounts/companies', JSON.stringify(data));
  }
}
