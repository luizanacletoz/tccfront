import {HttpClient} from '../app.interceptor';
import {Injectable} from '@angular/core';

@Injectable()
export class NotificationService {

  constructor(private _http: HttpClient) {
  }

  getAll() {
    return this._http.get('notifications');
  }
}
