import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginData: LoginData = {
    email: '',
    password: ''
  };
  public validationErrors = {
    email: [],
    password: []
  };

  public loading: true;

  constructor(
    public authService: AuthService,
    public router: Router) {
  }


  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.router.navigate(['dashboard/budget']);
    }
  }

  login() {
    this.authService
      .auth(this.loginData)
      .subscribe(res => {
        sessionStorage.setItem('token', JSON.stringify(res.json()));

        this.authService.info()
          .subscribe(res1 => {
            sessionStorage.setItem('user', JSON.stringify(res1.json()));
            this.router.navigate(['dashboard/budget']);
          }, err => {
            console.log(err);
            sessionStorage.clear();
            this.router.navigate(['login']);
          });
      }, err => {
        this.validationErrors = err.json();
      });
  }
}
