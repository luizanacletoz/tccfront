import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {EventsService} from 'angular4-events';
import {AuthService} from '../../services/auth.service';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public user: any;
  public numNotification: number;

  constructor(
    public router: Router,
    public events: EventsService,
    public authService: AuthService,
    public notificationService: NotificationService) { }



  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    if (!this.user) {
      this.logout();
    }

    this.getNotificationNumber();

    this.events.subscribe('remove-notification-num', () => {
      this.numNotification = null;
    });

    this.events.subscribe('update-user', () => {
      this.authService.info()
        .subscribe(res => {
          this.user = res.json();
          sessionStorage.setItem('user', JSON.stringify(res.json()));
        }, err => {
          this.logout();
        });
    });
  }

  getNotificationNumber() {
    this.notificationService.getAll()
      .subscribe(res => {
        const len = res.json().length;
        if (len !== 0) {
          this.numNotification = len;
        }
      }, err => {
        console.log(err);
      });
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['']);
  }

}
