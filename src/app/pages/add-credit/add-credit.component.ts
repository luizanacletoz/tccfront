import { Component, OnInit } from '@angular/core';
import {CreditService} from '../../services/credit.service';
import {EventsService} from 'angular4-events';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-credit',
  templateUrl: './add-credit.component.html',
  styleUrls: ['./add-credit.component.css']
})
export class AddCreditComponent implements OnInit {

  public loading: boolean;
  public validationErrors;

  constructor(public creditService: CreditService,
              public events: EventsService,
              public router: Router) {
    this.loading = false;
  }

  public placeholders: {
    number: '•••• •••• •••• ••••',
    name: 'Nome completo',
    expiry: '••/••••',
    cvc: '•••'
  };
  // (Visa / Master / Amex / Elo / Aura / JCB / Diners / Discover / Hipercard).
  public buyCredits = {
    credits: '',
    installments: 1,
    creditCard: {
      cardNumber: '',
      holder: '',
      expirationDate: '',
      securityCode: '',
      brand: null
    }
  };

  detectCardType() {
    const re = {
      Visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
      Master: /^5[1-5][0-9]{14}$/,
      Amex: /^3[47][0-9]{13}$/,
      Diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
      Discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
      JCB: /^(?:2131|1800|35\d{3})\d{11}$/
    };

    if (this.buyCredits.creditCard.cardNumber == '0000 0000 0000 0001') {
      this.buyCredits.creditCard.brand = 'master';
    }

    for (const key in re) {
      if (re[key].test(this.buyCredits.creditCard.cardNumber)) {
        this.buyCredits.creditCard.brand = key;
      }
    }
  }

  sendData() {
    this.loading = true;
    this.detectCardType();
    this.buyCredits.creditCard.expirationDate = this.buyCredits.creditCard.expirationDate.replace(' ', '').replace(' ', '');
    this.creditService
      .sendData(this.buyCredits)
      .subscribe(res => {
        this.loading = false;
        this.events.publish('update-user');
        this.router.navigate(['dashboard/budget']);
      }, err => {
        this.loading = false;
        this.validationErrors = err.json();
      });
  }



  ngOnInit() {
  }

}
