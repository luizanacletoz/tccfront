import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCompanyDataComponent } from './change-company-data.component';

describe('ChangeCompanyDataComponent', () => {
  let component: ChangeCompanyDataComponent;
  let fixture: ComponentFixture<ChangeCompanyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCompanyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCompanyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
