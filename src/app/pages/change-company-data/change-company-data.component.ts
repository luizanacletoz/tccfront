import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {CompanyService} from '../../services/company.service';
import {EventsService} from 'angular4-events';

@Component({
  selector: 'app-change-company-data',
  templateUrl: './change-company-data.component.html',
  styleUrls: ['./change-company-data.component.css']
})
export class ChangeCompanyDataComponent implements OnInit {
  constructor(
    public authService: AuthService,
    public router: Router,
    public companyService: CompanyService,
    public events: EventsService) {
  }

  public loading = false;
  public editPassword = false;
  public editData = false;

  public message;

  public changePassword = {
    currentPassword: '',
    password: '',
    samePassword: ''
  };

  public company = {
    name: '',
    email: '',
    cnpj: '',
    password: '',
    lat: null,
    long: null,
    address : {
      cep: '',
      uf: '',
      city: '',
      neighborhood: '',
      street: ''
    }
  };

  public validationErrors = {
    name: [],
    email: [],
    cnpj: [],
    password: [],
    lat: [],
    long: [],
    cep: [],
    uf: [],
    city: [],
    neighborhood: [],
    street: []
  };

  public passwordValidations;

  toggleDataMode() {
    this.editData = !this.editData;
  }

  togglePasswordMode() {
    this.editPassword = !this.editPassword;
  }

  ngOnInit() {
    this.loading = true;
    this.authService.info()
      .subscribe(res => {
        this.company = res.json();
        this.loading = false;
      }, err => {
        this.loading = false;
        const error = err.json();
        this.validationErrors = error;
        if (error.status === 403) {
          this.logout();
        }
      });
  }

  launchMessage(message){
   this.message = message;
   setTimeout(() => {
     this.message = '';
   }, 3000);
  }

  sendDataPassword(){
    this.authService.changePassword(this.changePassword)
      .subscribe(res => {
        this.togglePasswordMode();
        this.launchMessage('Senha alterada com sucesso!');
      }, err => {
        this.passwordValidations = err.json();
      });
  }

  sendData() {
    this.companyService.put({
      name: this.company.name,
      cep : this.company.address.cep,
      uf: this.company.address.uf,
      city: this.company.address.city,
      neighborhood: this.company.address.neighborhood,
      street: this.company.address.street
    }).subscribe(res => {
      this.events.publish('update-user');
      this.toggleDataMode();
      this.launchMessage('Dados alterados com sucesso!');

    }, err => {
      this.validationErrors = err.json();
    });
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['']);
  }
}
