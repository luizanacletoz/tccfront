import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../services/notification.service';
import {Router} from '@angular/router';
import {EventsService} from 'angular4-events';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  constructor(
    public notificationService: NotificationService,
    public router: Router,
    public events: EventsService) { }

  public loading = false;
  public notifications = [];

  ngOnInit() {
    this.loading = true;
    this.notificationService.getAll()
      .subscribe(res => {
        this.notifications = res.json();
        this.events.publish('remove-notification-num');
        this.loading = false;
      }, err => {
        this.loading = false;
      });
  }

  goToDetail(id) {
    this.router.navigate(['dashboard/budget/' + id]);
  }

}
