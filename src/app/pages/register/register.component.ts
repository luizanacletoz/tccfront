import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { Router } from '@angular/router';
import {ViacepService} from '../../services/viacep.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public regButton = false;
  public geolocalizationError = '';
  public loading = false;

  public company: Company = {
    name: '',
    email: '',
    cnpj: '',
    password: '',
    lat: null,
    long: null,
    cep: '',
    uf: '',
    city: '',
    neighborhood: '',
    street: ''
  };

  public validationErrors = {
    name: [],
    email: [],
    cnpj: [],
    password: [],
    lat: [],
    long: [],
    cep: [],
    uf: [],
    city: [],
    neighborhood: [],
    street: []
  };

  constructor(
    public companyService: CompanyService,
    public router: Router,
    public viacepService: ViacepService) { }

  ngOnInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(res => {
        this.company.lat =  res.coords.latitude;
        this.company.long = res.coords.longitude;

      }, err => {
        this.geolocalizationError = 'Para se cadastrar na plataforma, é necessário atiavar a geolocalização!';
        this.regButton = true;
      });
    }
  }

  getCep() {
    this.viacepService.get(this.company.cep)
      .subscribe(res => {
        const data = res.json();
        this.company.street = data.logradouro;
        this.company.city = data.localidade;
        this.company.neighborhood = data.bairro;
        this.company.uf = data.uf;
      }, err => {
        console.log('adad');
      });
  }

  register() {
    this.loading = true;
    this.companyService.register(this.company)
      .subscribe(res => {
        this.loading = false;
        this.router.navigate(['register-success']);
      }, err => {
        this.loading = false;
        this.validationErrors = err.json();
      });
  }

}
