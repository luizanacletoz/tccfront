import { Component, OnInit } from '@angular/core';
import {BudgetService} from '../../services/budget.service';
import {ActivatedRoute, Router} from '@angular/router';
import {forEach} from '@angular/router/src/utils/collection';
import {style} from '@angular/animations';
import {MzToastService} from 'ngx-materialize';
import { EventsService } from 'angular4-events';

@Component({
  selector: 'app-budget-detail',
  templateUrl: './budget-detail.component.html',
  styleUrls: ['./budget-detail.component.css']
})
export class BudgetDetailComponent implements OnInit {

  private readonly budgetId: any;
  public user: any;

  public budget: any;

  public loading: boolean;
  public proposalItems = [];
  public error: string;
  public validationErrors;

  constructor(public budgetService: BudgetService,
              public activatedRoute: ActivatedRoute,
              public router: Router,
              public toastService: MzToastService,
              public events: EventsService) {
    this.budgetId = this.activatedRoute.snapshot.paramMap.get('id');
    this.user = JSON.parse(sessionStorage.getItem('user'));
  }

  ngOnInit() {
    this.loading = true;
    this.budgetService
      .getById(this.user.id, this.budgetId)
      .subscribe(res => {
        this.budget = res.json();
        this.proposalItems = this.budget.items;
        this.proposalItems.forEach(x => {
          x.haveItem = true;
          x.checked = false;
        });

        this.loading = false;
      }, err => {
        this.loading = false;
        const error = err.json();
        if (error.status === 403) {
          this.logout();
        }
      });
  }

  sendData() {
    let responseItems = [];
    let haveNotChecked = false;
    let totalValue = 0;
    let haveOneItem = false;
    this.proposalItems.forEach((el, i) => {
      if (!el.checked) {
        el.err = true;
        haveNotChecked = true;
      } else {
        haveOneItem = true;
        responseItems.push({
          name: el.name,
          unityValue: el.unityValue,
          haveItem: el.haveItem,
          amount: el.amount,
          totalValue: el.totalValue
        });
        totalValue += el.totalValue;
      }
    });

    if (!haveOneItem) {
      this.error = 'Deve haver ao menos um produto na proposta do orçamento';
    }

    if (!haveNotChecked) {
      this.loading = true;
      var data = {
        budgetId: this.budgetId,
        responseItems: responseItems,
        value: totalValue
      };

      this.proposalItems.forEach((el, i) => {
        if(!el.haveItem){
          el.amount = 0;
        }
      });

      this.budgetService.createBudgetResponse(data).subscribe(res => {
        this.loading = false;
        this.events.publish('update-user');
        this.router.navigate(['dashboard/budget'])
      }, err => {
        this.loading = false;
        this.validationErrors = err.json();
        if(this.validationErrors.status == 403){
          this.logout();
        }
      });
    }
  }

  updateTotalValue(item) {
    this.error = '';
    if (typeof item.unityValue === 'number') {
      item.totalValue = item.amount * item.unityValue;
      item.checked = true;
      item.err = false;
      item.displayTotal = Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
      }).format(item.totalValue);
    }
  }

  toggleHaveItem(item) {
    this.error = '';
    item.haveItem = !item.haveItem;
    if (!item.haveItem) {
      item.unityValue = 0;
      item.totalValue = 0;
      item.displayTotal = 0;
      item.checked = true;
      item.err = false;
    } else {
      item.checked = false;
    }
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['']);
  }

}
