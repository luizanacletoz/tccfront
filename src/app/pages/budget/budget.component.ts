import { Component, OnInit } from '@angular/core';
import {BudgetService} from '../../services/budget.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent implements OnInit {

  constructor(public budgetService: BudgetService,
              public router: Router) {
  }

  public companyId: string;
  public budgets: any;
  public budgetsAnswered: any;
  public loading: boolean;
  public loadingAnswered: boolean;

  ngOnInit() {
    if (sessionStorage.getItem('user')){
      this.companyId = JSON.parse(sessionStorage.getItem('user')).id;
      this.getAll();
      this.getAllAnswered();
    }

  }

  getAll() {
    this.loading = true;
    this.budgetService.getAll(this.companyId)
      .subscribe(res => {
        this.budgets = res.json();
        this.loading = false;
      }, err => {
        this.loading = false;
        sessionStorage.clear();
        this.router.navigate(['login']);
      });
  }
  getAllAnswered() {
    this.loadingAnswered = true;
    this.budgetService.getAll(this.companyId, true)
      .subscribe(res => {
        this.budgetsAnswered = res.json();
        this.loadingAnswered = false;
      }, err => {
        this.loadingAnswered = false;
        sessionStorage.clear();
        this.router.navigate(['login']);
      });
  }
}
