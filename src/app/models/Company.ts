class Company {
  name: string;
  email: string;
  password: string;
  cnpj: string;
  lat: number;
  long: number;
  cep: string;
  uf: string;
  city: string;
  neighborhood: string;
  street: string;

}
