import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import {RouterModule} from '@angular/router';
import { ROUTES } from './app.routes';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import {HttpClient} from './app.interceptor';
import {AuthGuard} from './guards/auth.guard';
import {FormsModule} from '@angular/forms';
import {AuthService} from './services/auth.service';
import {HttpModule} from '@angular/http';
import { RegisterComponent } from './pages/register/register.component';
import {CompanyService} from './services/company.service';
import { RegisterSuccessComponent } from './pages/register-success/register-success.component';
import { BudgetComponent } from './pages/budget/budget.component';
import { ChangeCompanyDataComponent } from './pages/change-company-data/change-company-data.component';
import {MzButtonModule, MzCollapsibleModule, MzInputModule} from 'ngx-materialize';
import { AddCreditComponent } from './pages/add-credit/add-credit.component';
import {BudgetService} from './services/budget.service';
import { BudgetDetailComponent } from './pages/budget-detail/budget-detail.component';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import { MzToastModule } from 'ngx-materialize'
import {CardModule} from 'ngx-card';
import {CreditService} from './services/credit.service';
import {EventsModule} from 'angular4-events';
import {ViacepService} from './services/viacep.service';
import { NotificationComponent } from './pages/notification/notification.component';
import {NotificationService} from './services/notification.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent,
    RegisterSuccessComponent,
    BudgetComponent,
    ChangeCompanyDataComponent,
    AddCreditComponent,
    BudgetDetailComponent,
    NotificationComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    FormsModule,
    BrowserAnimationsModule,
    MzButtonModule,
    MzInputModule,
    CurrencyMaskModule,
    MzToastModule,
    CardModule,
    MzCollapsibleModule,
    EventsModule
  ],
  providers: [
    HttpClient,
    AuthGuard,
    AuthService,
    CompanyService,
    CreditService,
    BudgetService,
    ViacepService,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
