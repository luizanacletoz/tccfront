
import { Routes } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {AuthGuard} from './guards/auth.guard';
import {RegisterComponent} from './pages/register/register.component';
import {RegisterSuccessComponent} from './pages/register-success/register-success.component';
import {BudgetComponent} from './pages/budget/budget.component';
import {AddCreditComponent} from './pages/add-credit/add-credit.component';
import {ChangeCompanyDataComponent} from './pages/change-company-data/change-company-data.component';
import {BudgetDetailComponent} from './pages/budget-detail/budget-detail.component';
import {NotificationComponent} from './pages/notification/notification.component';

export const ROUTES: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' },

  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'register-success', component: RegisterSuccessComponent},

  { path: 'dashboard', component: DashboardComponent, canActivate : [ AuthGuard ], children: [
      { path: 'budget', component: BudgetComponent },
      { path: 'credit', component: AddCreditComponent },
      { path: 'myAccount', component: ChangeCompanyDataComponent },
      { path: 'notification', component: NotificationComponent },
      { path: 'budget/:id', component: BudgetDetailComponent },
    ]},
/*
  { path: 'colors', loadChildren: './pages/colors-page/colors-page-module.module#ColorsPageModuleModule', canActivate: [AuthGuard] },

  { path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard] }*/
];
